package com.example.projetomoneysaver.money_saver;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Context context;
    private Button btLogin;
    private TextView txtMsg;
    private Boolean flag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        btLogin = (Button) findViewById(R.id.bt_main_login);
        txtMsg = (TextView) findViewById((R.id.txtMsg));
        flag = false;
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if(flag) {
                    txtMsg.setText("Clique para logar!");
                    flag=false;
                }
                else {
                    txtMsg.setText("Logado");
                    flag=true;
                }
            }
        });

    }
}
